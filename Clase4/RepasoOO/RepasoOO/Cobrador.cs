﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepasoOO
{
    public class Cobrador
    {
        public virtual void Cobrar()
        {
            Console.WriteLine("Cobro un cobrador");
        }

        public static Cobrador Construir(string tipo)
        {
            if (tipo == "1")
                return new Cajero();
            if (tipo == "2")
                return new Supervisor();
            if (tipo == "3")
                return new Repositor();
            return new Cobrador();
        }
    }
}
