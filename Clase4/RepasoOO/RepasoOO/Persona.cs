﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RepasoOO
{
    public class Persona 
    {
        public Persona()
        {
            Console.WriteLine("Se construyo!");
        }
        public Persona(string nombre)
        {
            Console.WriteLine("Se construyo!" + nombre);
            this.Nombre = nombre;
        }

        ~Persona()
        {
            Console.WriteLine("Se Destruyo!");
        }

        public string Nombre { get; set; }
        public bool PiernaDerecha { get; set; }
        public bool PiernaIzquierda { get; set; }

        private void MoverPiernas(int milisegundos)
        {
            Console.WriteLine("Moviendo Derecha");
            PiernaDerecha = true;
            PiernaDerecha = false;

            Thread.Sleep(milisegundos / 2);

            Console.WriteLine("Moviendo Izquierda");
            PiernaIzquierda = true;
            PiernaIzquierda = false;

            Thread.Sleep(milisegundos / 2);
        }

        public void Caminar()
        {
            this.MoverPiernas(1000);
        }
        public void Caminar(int milisegundos)
        {
            if (milisegundos > 500)
                this.MoverPiernas(milisegundos);
            else
                Console.WriteLine("No se puede caminar tan rapido");
        }


        public void Correr()
        {
            this.MoverPiernas(500);
        }

        public void CorrerMuyRapido()
        {
            this.MoverPiernas(50);
        }

    }
}
