using Futbol.Futbol.Datos;
using Futbol.Futbol.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Futbol
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Jugador jugador1 = new Jugador();
            jugador1.Usuario = "Eduardo";
            jugador1.Password = "Mass";
            jugador1.Nombre = "Eduardo";
            jugador1.Apellido = "Mass";

            Jugador jugador2 = new Jugador();
            jugador2.Nombre = "Nicolas";
            jugador2.Apellido = "Gleizer";
            jugador2.Usuario = "Nico";
            jugador2.Password = "Nico123";

            Jugador jugador3 = new Jugador();
            jugador3.Nombre = "Gabriel";
            jugador3.Apellido = "Gorenstein";
            jugador3.Usuario = "Gabi";
            jugador3.Password = "Trokitos";

            using (Contexto context = new Contexto())
            {
                if (context.Jugadores.Count() == 0)
                {
                    context.Jugadores.Add(jugador1);
                    context.Jugadores.Add(jugador2);
                    context.Jugadores.Add(jugador3);
                    context.SaveChanges();
                }
            }

        }
    }
}

