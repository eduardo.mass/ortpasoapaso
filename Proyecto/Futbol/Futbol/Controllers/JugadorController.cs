﻿using Futbol.Futbol.Datos;
using Futbol.Futbol.Entidades;
using Futbol.Futbol.Reglas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Futbol.Controllers
{
    public class JugadorController : Controller
    {
        // GET: Jugador
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Agregar()
        {
            return View(new Jugador());
        }
        [HttpPost]
        public ActionResult Agregar(Jugador modelo)
        {
            RNJugador.Agregar(modelo);
            return View(modelo);
        }
    }
}