﻿using Futbol.Futbol.Entidades;
using Futbol.Futbol.Reglas;
using Futbol.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Futbol.Controllers
{
    public class LogInController : Controller
    {
        // GET: LogIn
        public ActionResult Index()
        {
            return View();
        }

        public Jugador UsuarioLogueado {
            get
            {
                if (Session["Usuario"] == null)
                    return null;
                return (Jugador)Session["Usuario"];
            }
            set
            {
                Session["Usuario"] = value;
            }
        }

        public ActionResult Loguearse(LogInModel modelo)
        {
            //if (modelo.NombreUsuario.Equals("Eduardo") &&
            //    modelo.Password.Equals("Mass"))

            //foreach (Jugador jugador in RNJugador.Buscar())

            if (this.UsuarioLogueado != null)
                return RedirectToAction("Index", "Partido");

            var jugador = RNJugador.Buscar(modelo.NombreUsuario, modelo.Password);
            if (jugador == null)
            {
                return View("Index");
            }

            this.UsuarioLogueado = jugador;

            return RedirectToAction("Index", "Partido");
        }

        public string Index2()
        {
            return "Hola mundo";
        }
    }
}