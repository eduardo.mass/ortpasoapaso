﻿using Futbol.Futbol.Reglas;
using Futbol.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Futbol.Controllers
{
    public class PartidoController : Controller
    {
        // GET: Paritido
        public ActionResult Index()
        {
            var partido = RNPartido.ObtenerPartidoActual();
            var listaJugadores = partido.ListaJugadores;
            
            PartidoModel modelo = new PartidoModel();
            modelo.FechaPartido = partido.FechaInicio;
            modelo.IdPartido = partido.Id;
            modelo.ListaJugadores = listaJugadores;
            if (modelo.ListaJugadores == null)
                modelo.ListaJugadores = new List<Futbol.Entidades.Jugador>();
            modelo.NombreJugadorLogueado = "Hola que tal soy el jugador logueado?";
            return View(modelo);
        }

        [HttpGet]
        public ActionResult Agregarme()
        {
            RNPartido.AgregarME();
            return RedirectToAction("Index", "Partido");
        }
    }
}