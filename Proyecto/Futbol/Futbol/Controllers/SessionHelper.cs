﻿using Futbol.Futbol.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Futbol.Controllers
{
    public class SessionHelper : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (SessionHelper.UsuarioLogueado == null)
            {
                if (filterContext.Controller is LogInController == false)
                {
                    filterContext.HttpContext.Response.Redirect("/Login/Index");
                }
            }
        }
        public static Jugador UsuarioLogueado
        {
            get
            {
                if (HttpContext.Current.Session["Usuario"] == null)
                    return null;
                return (Jugador)HttpContext.Current.Session["Usuario"];
            }
            set
            {
                HttpContext.Current.Session["Usuario"] = value;
            }
        }

        public static bool ExisteAlguienLogueado
        {
            get
            {
                return (SessionHelper.UsuarioLogueado != null);
            }
        }
    }
}