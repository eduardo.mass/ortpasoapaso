﻿using Futbol.Futbol.Datos;
using Futbol.Futbol.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Futbol.Futbol.Reglas
{
    public static class RNJugador
    {
        public static List<Jugador> Buscar()
        {
            return ADJugador.Buscar();
        }
        public static Jugador Buscar(string usuario, string password)
        {
            return ADJugador.Buscar(usuario, password);
        }
        public static Jugador Agregar(Jugador jugador)
        {
            var existeUsuarioMismoUserName = ADJugador.CuantosHay(jugador.Usuario);
            if (existeUsuarioMismoUserName)
            {
                throw new Exception("El nombre de usuario que desea usar ya existe");
            }
            return ADJugador.Agregar(jugador);

        }

        internal static Jugador Buscar(int id)
        {
            using (Contexto context = new Contexto())
            {
                return context.Jugadores.Where(o => o.Id == id).FirstOrDefault();
            }
        }
    }
}