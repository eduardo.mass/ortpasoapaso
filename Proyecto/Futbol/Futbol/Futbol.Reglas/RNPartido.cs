﻿using Futbol.Controllers;
using Futbol.Futbol.Datos;
using Futbol.Futbol.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Futbol.Futbol.Reglas
{
    public static class RNPartido
    {
        public static List<Partido> Buscar()
        {
            return ADPartido.Buscar();
        }
        public static Partido Agregar(Partido partido)
        {
            return ADPartido.Agregar(partido);
        }

        public static Partido ObtenerPartidoActual()
        {
            Partido ultimoPartidoIngresado = ADPartido.BuscarUltimoPartido();
            if (ultimoPartidoIngresado == null)
            {
                DateTime today = DateTime.Today;
                int daysUntilTuesday = ((int)DayOfWeek.Thursday -
                    (int)today.DayOfWeek + 7) % 7;
                DateTime nextTuesday = today.AddDays(daysUntilTuesday);

                var partido = ADPartido.Agregar(new Partido()
                {
                    FechaInicio = nextTuesday
                });
                return partido;
            }
            else
            {
                return ultimoPartidoIngresado;
            }
        }

        internal static void AgregarME()
        {
            Partido partido = RNPartido.ObtenerPartidoActual();
            ADPartido.AgegarJugadorNuevo(partido.Id, SessionHelper.UsuarioLogueado.Id);
        }

        public static void Modificar(Partido partido)
        {
            
        }
    }
}