﻿using Futbol.Futbol.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Futbol.Futbol.Datos
{
    public class ADJugador
    {
        public static List<Jugador> Buscar()
        {
            return new Contexto().Jugadores.ToList();
            //return BaseDeDatos.ListaJugadores;
        }
        public static Jugador Agregar(Jugador jugador)
        {
            Contexto contexto = new Contexto();
            contexto.Jugadores.Add(jugador);
            contexto.SaveChanges();

            BaseDeDatos.ListaJugadores.Add(jugador);
            return jugador;
        }

        public static Jugador Buscar(string usuario, string password)
        {

            return new Contexto().Jugadores.Where(
                jugador => jugador.Password == password &&
                jugador.Usuario == usuario
                ).FirstOrDefault();

            //return BaseDeDatos.ListaJugadores.Where(
            //    jugador=>jugador.Password == password &&
            //    jugador.Usuario == usuario
            //    ).FirstOrDefault();
        }
        public static bool CuantosHay(string usuario)
        {
            return BaseDeDatos.ListaJugadores.Any(
                jugador => jugador.Usuario == usuario);
        }
    }
}