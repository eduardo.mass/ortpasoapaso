﻿using Futbol.Futbol.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Futbol.Futbol.Datos
{
    public class ADPartido
    {
        public static List<Partido> Buscar()
        {
            return new Contexto().Partidos.ToList();
            //return BaseDeDatos.ListaJugadores;
        }
        public static Partido Agregar(Partido partido)
        {
            Contexto contexto = new Contexto();
            contexto.Partidos.Add(partido);
            contexto.SaveChanges();
            return partido;
        }

        public static Partido BuscarUltimoPartido()
        {
            using (Contexto contexto = new Contexto())
            {
                return contexto.Partidos
                    .OrderByDescending(o => o.FechaInicio)
                    .Include(b => b.ListaJugadores)
                    .FirstOrDefault();
            }
        }

        public static Jugador Buscar(string usuario, string password)
        {

            return new Contexto().Jugadores.Where(
                jugador => jugador.Password == password &&
                jugador.Usuario == usuario
                ).FirstOrDefault();

            //return BaseDeDatos.ListaJugadores.Where(
            //    jugador=>jugador.Password == password &&
            //    jugador.Usuario == usuario
            //    ).FirstOrDefault();
        }
        public static bool CuantosHay(string usuario)
        {
            return BaseDeDatos.ListaJugadores.Any(
                jugador => jugador.Usuario == usuario);
        }

        internal static void AgegarJugadorNuevo(int idPartido, int idJugador)
        {
            using (Contexto contexto = new Contexto())
            {
                var partidoDB = contexto.Partidos.Where(o => o.Id == idPartido).FirstOrDefault();
                var jugador = contexto.Jugadores.Where(o => o.Id == idJugador).FirstOrDefault();
                if (partidoDB.ListaJugadores != null)
                    partidoDB.ListaJugadores = new List<Jugador>();
                partidoDB.ListaJugadores.Add(jugador);
                contexto.SaveChanges();
            }

        }
    }
}