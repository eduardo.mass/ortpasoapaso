﻿using Futbol.Futbol.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Futbol.Futbol.Datos
{
    public static class BaseDeDatos
    {
        public static List<Jugador> ListaJugadores
        {
            get;
            set;
        } = new List<Jugador>();
    }
}