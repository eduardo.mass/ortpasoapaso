﻿using Futbol.Futbol.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Futbol.Futbol.Datos
{
    public class Contexto : DbContext
    {
        public DbSet<Jugador> Jugadores { get; set; }
        public DbSet<Partido> Partidos { get; set; }

    }
}