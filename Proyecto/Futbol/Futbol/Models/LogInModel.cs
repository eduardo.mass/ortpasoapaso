﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Futbol.Models
{
    public class LogInModel
    {
        public string NombreUsuario { get; set; }
        public string Password { get; set; }
    }
}