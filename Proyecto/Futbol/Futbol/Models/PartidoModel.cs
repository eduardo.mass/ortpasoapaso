﻿using Futbol.Futbol.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Futbol.Models
{
    public class PartidoModel
    {
        public List<Jugador> ListaJugadores { get; set; }
        public string NombreJugadorLogueado { get; set; }
        public int IdPartido { get; set; }
        public DateTime FechaPartido { get; set; }

    }
}