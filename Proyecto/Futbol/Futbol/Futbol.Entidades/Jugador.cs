﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Futbol.Futbol.Entidades
{
    public class Jugador
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public List<Partido> ListaPartidos { get; set; }
        public int Velocidad { get; set; }
        public int Pases { get; set; }

        public string Rol { get; set; }

    }
}