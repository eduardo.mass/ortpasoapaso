﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Futbol.Futbol.Entidades
{
    public class Partido
    {
        public int Id { get; set; }
        public DateTime FechaInicio { get; set; }
        public List<Jugador> ListaJugadores { get; set; }
    }
}