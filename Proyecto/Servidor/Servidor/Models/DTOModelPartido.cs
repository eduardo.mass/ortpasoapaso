﻿using Servidor.Servidor.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Servidor.Models
{
    public class DTOModelPartido
    {
        public List<Jugador> Jugadores { get; set; }
        public DateTime FechaPartido { get; set; }
        public Jugador Jugador { get; set; }

    }
}