﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Servidor.Servidor.Entidades
{
    public class Equipo
    {
        public int Id { get; set; }
        public List<Jugador> ListaJugadores { get; set; } = new List<Jugador>();
    }
}