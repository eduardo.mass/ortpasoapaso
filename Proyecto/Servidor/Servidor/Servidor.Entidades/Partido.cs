﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Servidor.Servidor.Entidades
{
    public class Partido
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public List<Jugador> ListaJugadoresConfirmados { get; set; } = new List<Jugador>();
        

        public void SetGanador(Equipo equipo)
        {

        }
        public void AgregarJugador(Jugador jugador)
        {

        }
        public void QuitarJugador(Jugador jugador)
        {

        }
        public void Empezar()
        {

        }
        public void Cancelar()
        {

        }

    }
}