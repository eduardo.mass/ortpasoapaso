using Servidor.Servidor.Datos;
using Servidor.Servidor.Entidades;
using Servidor.Servidor.Reglas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace Servidor
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            BaseDatos.ListaJugadores.Add(new Jugador()
            {
                Id = 1,
                Nombre = "Eduardo",
                Password = "Mass"
            }
            );
            BaseDatos.ListaJugadores.Add(new Jugador()
            {
                Id = 2,
                Nombre = "Gabriel",
                Password = "Gorenstein"
            }
            );
            BaseDatos.ListaJugadores.Add(new Jugador()
            {
                Id = 3,
                Nombre = "Ezequiel",
                Password = "Galansky"
            }
            );
            BaseDatos.ListaJugadores.Add(new Jugador()
            {
                Id = 4,
                Nombre = "Alejandro",
                Password = "Abadi"
            }
            );
            BaseDatos.ListaJugadores.Add(new Jugador()
            {
                Id = 5,
                Nombre = "Nicolas",
                Password = "Gleizer"
            }
            );
            BaseDatos.ListaJugadores.Add(new Jugador()
            {
                Id = 6,
                Nombre = "Bicho",
                Password = "Neiman"
            }
            );
            DateTime ahora = DateTime.Now;
            for (; ahora.DayOfWeek  != DayOfWeek.Thursday;)
            {
                ahora = ahora.AddDays(-1);
            }

            BaseDatos.ListaPartidos.Add(new Partido()
            {
                Fecha  = ahora, Id  = 4
            });
            
            BaseDatos.ListaPartidos.Add(new Partido()
            {
                Fecha = ahora.AddDays(-7),
                Id = 3
            });
            BaseDatos.ListaPartidos.Add(new Partido()
            {
                Fecha = ahora.AddDays(-7 * 2),
                Id = 2
            });
            BaseDatos.ListaPartidos.Add(new Partido()
            {
                Fecha = ahora.AddDays(-7 * 3),
                Id = 1
            });

            var partido = RNPartido.ObtenerPartidoActual();
            var jugador1 =RNJugador.Buscar(4);
            var jugador2 = RNJugador.Buscar(3);
            partido.ListaJugadoresConfirmados.Add(jugador1);
            partido.ListaJugadoresConfirmados.Add(jugador2);
        }
    }
}
