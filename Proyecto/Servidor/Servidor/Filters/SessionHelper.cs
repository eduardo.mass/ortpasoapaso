﻿using Servidor.Controllers;
using Servidor.Servidor.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace Servidor.Filters
{
    public class SessionHelper : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (SessionHelper.JugadorLogueado == null)
            {
                if (filterContext.Controller is LogInController == false)
                {
                    filterContext.HttpContext.Response.Redirect("/Login/Index");
                }
            }
        }

        public static Jugador JugadorLogueado
        {
            get
            {
                if (HttpContext.Current.Session["Usuario"] == null)
                    return null;
                return (Jugador)HttpContext.Current.Session["Usuario"];
            }
            set
            {
                HttpContext.Current.Session["Usuario"] = value;
            }
        }
    }
}