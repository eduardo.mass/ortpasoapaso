﻿using Servidor.Servidor.Datos;
using Servidor.Servidor.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Servidor.Servidor.Reglas
{
    public class RNPartido
    {
        public static List<Partido> Buscar()
        {
            return ADPartido.Buscar();
        }
        public static Partido ObtenerPartidoActual()
        {
            var partido = ADPartido.BuscarPartidoMasActual();
            return partido;
        }

    }
}