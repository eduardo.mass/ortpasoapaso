﻿using Servidor.Servidor.Datos;
using Servidor.Servidor.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Servidor.Servidor.Reglas
{
    public class RNJugador
    {
        public static List<Jugador> Buscar()
        {
            return ADJugador.Buscar();
        }
        public static Jugador Buscar(int id)
        {
            return ADJugador.Buscar(id);
        }
        public static Jugador ObtenerJugador(string usuario, string password)
        {
            return ADJugador.Buscar(usuario, password);
        }

    }
}