﻿using Servidor.Filters;
using Servidor.Models;
using Servidor.Servidor.Reglas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Servidor.Controllers
{
    public class LogInController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login(DTOLogIn dto)
        {
            var usuario = RNJugador.ObtenerJugador(dto.User, dto.Password);
            if (usuario == null)
                return View("Index");

            SessionHelper.JugadorLogueado= usuario;

            return RedirectToAction("Index", "Partido");
        }

    }
}