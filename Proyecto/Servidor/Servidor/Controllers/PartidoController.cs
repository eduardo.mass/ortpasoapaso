﻿using Servidor.Filters;
using Servidor.Models;
using Servidor.Servidor.Datos;
using Servidor.Servidor.Reglas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Servidor.Controllers
{
    public class PartidoController : Controller
    {
        // GET: Partido
        public ActionResult Index()
        {
            var partidoActual = RNPartido.ObtenerPartidoActual();
            DTOModelPartido modelo = new DTOModelPartido();
            modelo.Jugador = SessionHelper.JugadorLogueado;


            modelo.Jugadores = partidoActual.ListaJugadoresConfirmados;
            modelo.FechaPartido = partidoActual.Fecha;
            return View(modelo);
        }

    }
}
