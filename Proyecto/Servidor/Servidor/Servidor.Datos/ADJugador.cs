﻿using Servidor.Servidor.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Servidor.Servidor.Datos
{
    public class ADJugador
    {
        internal static List<Jugador> Buscar()
        {
            return BaseDatos.ListaJugadores;
        }
        internal static Jugador Buscar(int id)
        {
            return BaseDatos.ListaJugadores.Where(o=>o.Id == id).FirstOrDefault();
        }
        internal static Jugador Buscar(string usuario, string password)
        {
             return BaseDatos.ListaJugadores.Where(o=>o.Nombre == usuario &&
                    o.Password == password).FirstOrDefault();
        }
    }
}