﻿using Servidor.Servidor.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Servidor.Servidor.Datos
{
    public class ADPartido
    {
        internal static List<Partido> Buscar()
        {
            return BaseDatos.ListaPartidos;
        }
        internal static Partido BuscarPartidoMasActual()
        {
            return BaseDatos.ListaPartidos.OrderBy(o => o.Fecha).FirstOrDefault();
        }
    }
}