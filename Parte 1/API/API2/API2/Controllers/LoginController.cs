﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace API2.Controllers
{
    public class LoginController : ApiController
    {
        // GET: api/Login
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        [HttpGet]
        public void Iniciar(string nombre)
        {
            HttpContext.Current.Session["usuario"] = nombre;
        }
        [HttpGet]
        public void Login(string nombre)
        {
            HttpContext.Current.Session["usuario"] = nombre;
        }

        // GET: api/Login/5
        public string Get(int id)
        {
            return HttpContext.Current.Session["usuario"].ToString();
        }

        // POST: api/Login
        //public void Post([FromBody]string value)
        public HttpResponseMessage EjemploPost([FromBody]Modelo login)
        {
            
            var response = Request.CreateResponse(HttpStatusCode.Found);
            if (login.Nombre == "Eduardo" &&
                login.Password == "123")
                response.Headers.Location = new Uri("http://www.google.com"); //here you just need to pass appropriate link 
            else
                response.Headers.Location = new Uri("http://www.ole.com"); //here you just need to pass appropriate link 

            return response;
        }

        // PUT: api/Login/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Login/5
        public void Delete(int id)
        {
        }
    }

    public class Modelo
    {
        public string Nombre { get; set; }
        public string Password { get; set; }
    }
}
